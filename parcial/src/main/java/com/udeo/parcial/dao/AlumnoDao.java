package com.udeo.parcial.dao;
import java.util.List;

import com.udeo.parcial.Entity.Alumno;

public interface AlumnoDao {
	public List<Alumno> findAll();
	
	public Alumno findById(int id);

}
