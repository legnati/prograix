package com.udeo.parcial.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.udeo.parcial.Entity.Alumno;

@Repository
public class AlumnoDaoImpl implements AlumnoDao {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Alumno> findAll() {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Alumno> selectQuery = currentSession.createQuery("from alumno", Alumno.class);
		List<Alumno> alumnos = selectQuery.getResultList();
		return alumnos;
	}

	@Override
	public Alumno findById(int id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Alumno alumno = currentSession.get(Alumno.class, id);
		return alumno;
	}
}
