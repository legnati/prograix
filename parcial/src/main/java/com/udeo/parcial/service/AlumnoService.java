package  com.udeo.parcial.service;

import java.util.List;

import com.udeo.parcial.Entity.Alumno;

public interface AlumnoService {
	
	public List<Alumno> findAll();
	
	public Alumno findById(int id);

}
