package com.udeo.parcial.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.udeo.parcial.Entity.Alumno;
import com.udeo.parcial.dao.AlumnoDao;

 

@Service
public class AlumnoServiceImpl implements AlumnoService {

	@Autowired
	private AlumnoDao alumnoDAO;
	
	@Override
	public List<Alumno> findAll() {
		List<Alumno> listAlumnos= alumnoDAO.findAll();
		return listAlumnos;
	}

	@Override
	public Alumno findById(int id) {
		Alumno alumno = alumnoDAO.findById(id);
		return alumno;
	}

}