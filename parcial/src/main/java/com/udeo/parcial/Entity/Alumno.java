package com.udeo.parcial.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity(name="alumno")
@Table(name="alumno")
public class Alumno {
/*PK == Primary Key*/
	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	

	@Column(name="email")
	private String email;
	
	/*parametros nombrados*/
	public Alumno() {}
	public Alumno(int id, String first_name, String last_name, String email) {
		this.id = id;
		this.firstName = first_name;
		this.lastName = last_name;
		this.email = email;
	}
	/*GET*/
	
	public int getId() {
		return this.id;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public String getEmail() {
		return this.email;
	}

		
	/*OVERRIDE*/
/*	@Override
	public String toString() {
		return "Alumno [id]"+ id +", firstName= "+ firstName + ",lastName= "+ lastName + "email =" + email;
	}
*/
}
